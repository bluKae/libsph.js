import { decrypt, encrypt } from '../util/jcryption.mjs'
import { extractBackJsonResponseData, inspect } from '../util/responseutils.mjs'
import { serializeArray } from '../util/util.mjs'
import { buildSphLocation, buildSphRoute, encryptedForm, encryptedJson, postForm } from './sphroutes.mjs'

const location = buildSphLocation('start', 'nachrichten.php')

export function showMessage(sid, messageId) {
    const form = postForm({
        a: 'recycleMsg',
        uniqid: messageId,
    })

    return buildSphRoute(location, form, sid, async response => {
        const text = await response.text()
        
        if (text !== 'true')
            throw Error('error with hiding message')
    })
}

export function hideMessage(sid, messageId) {
    const form = postForm({
        a: 'deleteAll',
        uniqid: messageId,
    })

    return buildSphRoute(location, form, sid, inspect)
}

export function getMessageHeaders(sid, aesPassword, getHidden) {
    const form = postForm({
        a: 'headers',
        getType: getHidden ? 'unvisibleOnly' : 'visibleOnly',
        last: 0,
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        if (data.total === 0)
            return []

        const messages = JSON.parse(decrypt(data.rows, aesPassword))

        return messages.map(m => ({
            id: m.Uniquid,
            unreadReplies: m.unread,
            hidden: m.Papierkorb === 'ja',
            subject: m.Betreff,
            sender: {
                id: m.Sender,
                name: m.SenderName,
                shortening: m.kuerzel,
            },
            recipentGroup: m.WeitereEmpfaenger || null,
            recipients: m.empf,
            lastReplyDate: m.Datum,
            lastReplyTimestamp: m.DatumUnix,
        }))
    })
}

export function queryRecipientspts(sid, query, page) {
    const form = postForm({
        a: 'searchRecipt',
        q: query,
        page,
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        return data.items.map(i => ({
            ...i,
        }))
    })
}

export function createMessage(sid, aesPassword, recipients, subject, text) {
    const form = postForm({
        a: 'newmessage',
        c: encryptedJson(
            aesPassword,
            serializeArray({
                to: recipients,
                subject,
                text,
            }),
        ),
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await extractBackJsonResponseData(response)

        return data.id
    })
}

// export function getRecipients(sid, recipients) {
//     const form = postForm({
//         a: 'getRecipt',
//         q: recipients.join(','),
//     })

//     return buildSphRoute(location, form, sid, inspect)
// }

function parseReply(reply) {
    return {
        id: message.Uniquid,
        sender: {
            senderId: message.Sender,
            senderName: message.SenderName,
            senderUserName: message.username,
            senderType: message.SenderArt,
        },

        sentOn: message.Datum,
        content: message.Inhalt,
        own: message.own,
        answerAllowed: !message.noanswer,
        deleteScheduledAt: message.Delete,
        unreadCount: message.ungelesen,
    }
}

export function getMessage(sid, aesPassword, id) {
    const form = postForm({
        a: 'read',
        uniqid: encrypt(id, aesPassword),
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        if (data.back === false || data.error !== '0')
            throw Error("could not get message")

        const message = JSON.parse(decrypt(data.message, aesPassword))

        return {
            lastRefreshTimestamp: data.time,
            userId: data.userId,
            allowReplyToStudent: data.ToolOptions.AllowSuSToSuSMessages === 'on',
            userType: data.UserTyp,

            onlyGroupReplies: message.groupOnly === 'ja',
            onlyPrivateReplies: message.privateAnswerOnly === 'ja',
            answerAllowed: message.noAnswerAllowed === 'nein',
            stats: {
                students: message.statistik.teilnehmer,
                teachers: message.statistik.betreuer,
                parents: message.statistik.eltern,
            },
            participants: message.empf || new Array(message.private),
            participantGroup: message.WeitereEmpfaenger || null,
            replyToHidden: message.AntwortAufAusgeblendeteNachricht === 'on',
            subject: message.Betreff,
            hidden: message.Papierkorb === 'ja',
            unreadCount: message.ungelesen || 0,
            replies: [message, ...message.reply].map(parseReply),
            message,
        }
    })
}

export function refreshMessage(sid, aesPassword, id, lastRefresh) {
    const form = postForm({
        a: 'refresh',
        last: lastRefresh,
        uniqid: id,
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        const replies = JSON.parse(decrypt(data.reply, aesPassword))

        return {
            lastRefreshTimestamp: data.time,
            reply: replies.map(parseReply),
        }
    })
}

export function reply(sid, aesPassword, recipient, onlyGroupReplies, onlyPrivateReplies, content, messageId) {
    const form = postForm({
        a: 'reply',
        c: encryptedJson(aesPassword, {
            to: recipient ?? 'all',
            groupOnly: onlyGroupReplies ? 'nein' : 'ja',
            privateAnswerOnly: onlyPrivateReplies ? 'ja' : 'nein',
            message: content,
            replyToMsg: messageId,
        }),
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await extractBackJsonResponseData(response)

        return data.id
    })
}

export function countNewMessages(sid) {
    const form = postForm({
        a: 'countUnreadMesages',
    })

    return buildSphRoute(location, form, sid, async response => await response.json() || 0)
}