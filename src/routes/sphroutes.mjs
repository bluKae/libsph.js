import { encrypt } from '../util/jcryption.mjs'

export function buildSphLocation(subdomain, path) {
    return `https://${subdomain}.schulportal.hessen.de/${path}`
}

export function buildSphRoute(location, form, sid, parser) {
    if (form.query)
        location += `?${form.query}`

    const headers = { 
        'X-Requested-With': 'XMLHttpRequest',
    }

    if (form.body)
        headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

    if (sid)
        headers['Cookie'] = `sid=${sid}`

    return {
        url: location,
        body: form.body,
        method: form.body ? 'POST' : 'GET',
        headers,
        parser,
    }
}

export function getForm(params) {
    return { query: new URLSearchParams(params).toString() }
}

export function postForm(params) {
    return { body: new URLSearchParams(params) }
}

export function encryptedForm(aesPassword, params) {
    return encrypt(new URLSearchParams(params).toString(), aesPassword)
}

export function encryptedJson(aesPassword, object) {
    return encrypt(JSON.stringify(object), aesPassword)
}