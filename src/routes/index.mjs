import { extractCookieFromResponse, extractStringFromResponse } from '../util/responseutils.mjs'
import { getForm, buildSphLocation, buildSphRoute } from './sphroutes.mjs'


const location = buildSphLocation('start', 'index.php')

export function school(schoolId) {
    const form = getForm({
        i: schoolId,
    })

    return buildSphRoute(location, form, null, async response => ({
        sid: extractCookieFromResponse(response, 'sid'),
        ikey: await extractStringFromResponse(response, 'name="ikey" value="', '"'),
    }))
}

