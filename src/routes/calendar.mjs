import { extractBetween } from '../util/util.mjs'
import { buildSphLocation, buildSphRoute, postForm } from './sphroutes.mjs'

const location = buildSphLocation('start', 'kalender.php')

function getIsoDateOrReturn(date) {
    if (typeof date === 'string') return date
    return extractBetween(date.toISOString(), '', 'T')
}

async function queryEventsParser(response) {
    const data = await response.json()
    
    return data.map(i => ({
        id: i.Id,
        name: i.title,
        description: i.description ?? null,
        categoryId: parseInt(i.category),
        allDay: i.allDay,
        start: new Date(i.start),
        end: new Date(i.end),
        new: i.Neu === 'ja',
    }))
}

function queryEventsForm(queryNewEvents, categoryId, query) {
    return {
        f: 'getEvents',
        k: categoryId ?? '',
        s: query ?? '',
        ...(queryNewEvents ? { neu: 'true' } : {}),
    }
}

export function queryEvents(sid, from, to, queryNewEvents, categoryId, query) {
    const form = postForm({
        ...queryEventsForm(queryNewEvents, categoryId, query),
        start: getIsoDateOrReturn(from),
        end: getIsoDateOrReturn(to),
    })

    return buildSphRoute(location, form, sid, queryEventsParser)
}

export function queryYearlyEvents(sid, yearNumber, queryNewEvents, categoryId, query) {
    const form = postForm({
        ...queryEventsForm(queryNewEvents, categoryId, query),
        year: yearNumber,
        start: 'year',
        end: 'year',
    })

    return buildSphRoute(location, form, sid, queryEventsParser)
}

export function getEvent(sid, eventId) {
    const form = postForm({
        f: 'getEvent', 
        id: eventId,
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        if (!data.id)
            return

        return {
            id: data.id,
            name: data.title,
            start: new Date(data.start.date),
            end: new Date(data.end.date),
            allDay: data.allDay,
            host: data.properties.verantwortlich,
            description: (data.description && data.description !== ' ') ? data.description : null,
            location: (data.properties.ort && data.properties.ort !== 'null') ? data.properties.ort : null,
            groups: Object.values(data.properties.zielgruppen),
            createdOn: data.properties.created ?? null,
            categoryId: parseInt(data.category),
        }
    })
}

export function getIcalUrl(sid) {
    const form = postForm({
        f: 'iCalAbo',
    })

    return buildSphRoute(location, form, sid, async response => await response.text())
}

export function getCategories(sid) {
    return buildSphRoute(location, {}, sid, async response => {
        const html = await response.text()

        return Array.from(html.matchAll(/^  categories\.push\(\{ id: (\d*), name:'([^']*)', color:'#([0-9a-f]*)', logo:'([^']*)'} \);$/gm), m => ({
            id: parseInt(m[1]),
            name: m[2],
            color: m[3],
            logo: m[4],
        }))
    })
}