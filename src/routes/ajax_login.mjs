import { buildSphLocation, buildSphRoute, postForm } from './sphroutes.mjs'

const location = buildSphLocation('start', 'ajax_login.php')

export function validate(sid) {
    const form = postForm({
        name: sid,
    })

    return buildSphRoute(location, form, sid, async response => parseInt(await response.text()))
}