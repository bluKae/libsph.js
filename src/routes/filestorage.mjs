import { Parser } from 'htmlparser2'
import { inspect } from '../util/responseutils.mjs'
import { buildSphLocation, buildSphRoute, getForm, postForm } from './sphroutes.mjs'


const location = buildSphLocation('start', 'dateispeicher.php')

export const ROOT_FOLDER = 0

export function view(sid, folderId) {
    const form = getForm({
        a: 'view',
        folder: folderId,
    })

    return buildSphRoute(location, form, sid, async response => {
        const folders = []
        const files = []

        const p = new Parser({
            onopentag(tag, attribs) {
                if (tag === 'div' && attribs.class === 'thumbnail folder')
                    folders.push({
                        id: parseInt(attribs['data-id']),
                        name: attribs['data-name'],
                    })

                else if (tag === 'tr' && attribs['data-id']) {
                    files.push({
                        id: parseInt(attribs['data-id']),
                        extension: attribs['data-ext'],
                        name: attribs['data-name'],
                    })
                }
            }
        })

        p.write(await response.text())
        p.end()

        return { folders, files }
    })
}

export function download(sid, fileId) {
    const form = getForm({
        a: 'download',
        f: fileId,
    })

    return buildSphRoute(location, form, sid, inspect)
}

export function search(sid, query) {
    const form = postForm({
        a: 'searchFiles',
        q: query,
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await response.json()

        return data[0].map(f => ({
            id: parseInt(f.id),
            name: f.text,
            folder: parseInt(f.ordner),
        }))
    })
}