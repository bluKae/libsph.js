import { encryptKey } from '../util/jcryption.mjs'
import { extractBackJsonResponseData, extractCookieFromResponse, extractValueFromJsonResponse } from '../util/responseutils.mjs'
import { buildSphLocation, getForm, postForm, buildSphRoute, encryptedForm } from './sphroutes.mjs'


const location = buildSphLocation('start', 'ajax.php')

export function publicKey() {
    const form = getForm({
        f: 'rsaPublicKey',
    })

    return buildSphRoute(location, form, null, async response => await extractValueFromJsonResponse(response, 'publickey'))
}

export function handshake(sid, aesPassword, publicKey, randomNumber) {
    const form = {
        ...getForm({
            f: 'rsaHandshake',
            s: randomNumber,
        }),
        ...postForm({
            key: encryptKey(aesPassword, publicKey),
        }),
    }

    return buildSphRoute(location, form, sid, async response => await extractValueFromJsonResponse(response, 'challenge'))
}

export function login(sid, aesPassword, ikey, username, password) {
    const form = postForm({
        crypt: encryptedForm(aesPassword, {
            f: 'alllogin',
            art: 'all',
            sid: '',
            ikey,
            user: username,
            passw: password, 
        })
    })

    return buildSphRoute(location, form, sid, async response => {
        const data = await extractBackJsonResponseData(response)
        
        return {
            name: data.name,
            type: data.logo,
            sid: extractCookieFromResponse(response, 'sid'),
        }
    })
}

