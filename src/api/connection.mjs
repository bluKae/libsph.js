export class Connection {
    async enqueue(route) {
        return await this.send(route)
    }

    async send(route) {
        const response = await fetch(route.url, route)

        return await route.parser(response)
    }

    async inspect(route) {
        const response = await fetch(route.url, route).then(r => r.text())

        console.log(response)

        return response
    }
}
