import { handshake, login, publicKey } from '../routes/ajax.mjs'
import { validate } from '../routes/ajax_login.mjs'
import { school } from '../routes/index.mjs'
import { decrypt } from '../util/jcryption.mjs'
import { Connection } from './connection.mjs'

export class Session {
    constructor(connection, sid, ikey) {
        this.connection = connection
        this.sid = sid
        this.ikey = ikey
        this.password = null
    }

    static async fetch(schoolId, connection) {
        const { sid, ikey } = await connection.enqueue(school(schoolId))

        return new Session(connection, sid, ikey)
    }

    async handshake(password) {
        this.password = password

        const challenge = await this.connection.enqueue(handshake(this.sid, password, await this.connection.enqueue(publicKey()), Math.floor(Math.random * 2000)))

        this.password = decrypt(challenge, password) === password ? password : null
        
        return !!this.password
    }

    async login(username, password) {
        const { name, type, sid } = await this.connection.enqueue(login(this.sid, 'passwd', this.ikey, username, password))
        this.sid = sid
        return { name, type }
    }

    async validate() {
        return await this.connection.enqueue(validate(this.sid))
    }

    serialize() {
        return {
            sid: this.sid, 
            ikey: this.ikey, 
            password: this.password,
        }
    }

    static async from(data, check, connection) {
        const session = new Session(connection ?? new Connection(), data.sid, data.ikey)
        session.password = data.password

        return (!check || await session.validate()) ? session : null
    }
}