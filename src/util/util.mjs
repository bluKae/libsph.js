
export function extractBetween(string, start, end) {
    const startPos = start.length && string.indexOf(start) + start.length
    const endPos = end && string.indexOf(end, startPos)

    return string.substring(startPos, endPos)
} 

export function generate(generator, times) {
    const list = []

    for (let i = 0; i < times; i++) {
        const item = generator.next()
        if (item.done)
            break
        list.push(item.value)
    }

    return list
}

function nameVal(name, value) {
    return { name, value }
}

export function serializeArray(params) {
    return Object.entries(params).flatMap(([n, v]) => {
        if (!Array.isArray(v))
            return nameVal(n, v)

        return v.map(i => nameVal(n + '[]', i))
    })
}