
import { createCipheriv, createDecipheriv, createHash, publicEncrypt, randomBytes, constants } from 'crypto'
import { generate } from './util.mjs'

function* EVP_BytesToKey(password, salt) {
    let block;

    while (true) {
        const hash = createHash('md5')

        if (block)
            hash.update(block)

        hash.update(password)
        hash.update(salt)

        block = hash.digest()

        yield block
    }
}

function deriveKeyAndIv(password, salt) {
    const kdf = EVP_BytesToKey(Buffer.from(password, 'utf-8'), salt)

    return [
        Buffer.concat(generate(kdf, 2)), 
        kdf.next().value,
    ]
}

export function encrypt(string, password, salt) {
    if (!salt)
        salt = randomBytes(8)

    const [key, iv] = deriveKeyAndIv(password, salt)

    const cipher = createCipheriv('aes-256-cbc', key, iv)

    return Buffer.concat([
        Buffer.from('Salted__', 'utf-8'), 
        salt, 
        cipher.update(string, 'utf-8'), 
        cipher.final(),
    ]).toString('base64')
}

export function decrypt(ciphertext, password) {
    const b = Buffer.from(ciphertext, 'base64')

    if (b.subarray(0, 8).toString('utf-8') !== 'Salted__')
        throw Error('ciphertext does not start with "Salted__"')

    const [key, iv] = deriveKeyAndIv(password, b.subarray(8, 16))

    const decipher = createDecipheriv('aes-256-cbc', key, iv)

    return Buffer.concat([
        decipher.update(b.subarray(16)),
        decipher.final(),
    ]).toString('utf-8')
}

export function encryptKey(key, publicKey) {
    return publicEncrypt({
        key: publicKey,
        padding: constants.RSA_PKCS1_PADDING,
    }, Buffer.from(key, 'utf-8')).toString('base64')
}
