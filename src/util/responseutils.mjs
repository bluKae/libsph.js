import { extractBetween } from './util.mjs'


export async function inspect(response) {
    console.log(await response.text())
}

export function extractCookieFromResponse(response, cookieName) {
    const cookieLines = response.headers.get('set-cookie')

    const cookieLine = cookieLines && cookieLines.split(', ')
        .find(c => c.startsWith(cookieName))

    return cookieLine && extractBetween(cookieLine, '=', ';')
}

export async function extractValueFromJsonResponse(response, key) {
    const data = await response.json()

    return data[key]
}

export async function extractBackJsonResponseData(response) {
    const data = await response.json()

    if (!data.back)
        return Promise.reject()

    return data
}

export async function extractStringFromResponse(response, start, end) {
    const text = await response.text()

    return extractBetween(text, start, end)
}