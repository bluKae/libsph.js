# libsph.js

An unofficial client for schulportal.hessen.de written in JavaScript

## Architecture

The library consists mainly of two parts:

- route definitions
- api client

### Route Definitions

There are .mjs files in src/routes/ for each .php file on the server. Inside of these, there are functions defined for each route for the location respectively. The route definitions function as a wrapper for the undocumented and weired routes of the api. Session ids need to be given to the respective route and encryption as well as decryption is handled purely by it. It accepts easy to understand arguments and returns a route, which after sending returns the well formed representation of the api's output.

### Api Client

This is an object oriented wrapper for the route definitions. It implements caching, a ratelimiting https connection and automatic session management

## Todo / Goals

- [X] route definitions for logging in
- [X] most basic https connection
- [X] rewrite of the jCryption library
- [ ] ratelimiting of https connection
- [ ] route definitions for every route
- [ ] object oriented wrappers
- [ ] session management
- [ ] caching

My goal is creating a crossplatform mobile app in svelte native utilizing the api. If I have more time, I may even dedicate my time to writing a crossplatform desktop app.
