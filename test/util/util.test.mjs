import { extractBetween } from '../../src/util/util.mjs'

test('extracts bb from babbcd', () => {
    expect(extractBetween('babbcd', 'a', 'c')).toBe('bb')
})